Author: Daniel Goertz
Email: goertz.daniel@gmail.com

How-To-Use:
Type into terminal and type: java -jar CarMeqTest.jar /Path/To/Folder/

Used Software:
- MacOS Mojave (10.14.1)
- Java jdk-10.0.2
- IntelliJ IDEA
- Eclipse + ObjectAid (UML-Plugin)

Path to JAR file:
- ../CarMeqTest/out/artifacts/CarMeqTest_jar/CarMeqTest.jar

Path to UML:
- ../CarMeqTest/doc/UML/UML-Diagram.jpg

Path to JavaDoc:
- ../CarMeqTest/doc/JavaDoc/index.html

Path to Source Files:
- ../CarMeqTest/src/carmeqtest

Spend time: 
- Approx. a day
