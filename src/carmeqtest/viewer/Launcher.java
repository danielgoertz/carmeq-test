/**
 * 
 */
package carmeqtest.viewer;

import carmeqtest.controller.FileManager;
import carmeqtest.model.DataModel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author danielgoertz
 *
 */
public class Launcher {

	/**
	 * @param args: args[0] contains the file path to entry point.
	 */

	public static void main(String[] args) {

        var rootFilePathName = "";

        if(args.length == 0 ) {
			System.out.println("Missing path argument. Please try again e.g:'$ java -jar CarMeqTest.jar /Path/To/Folder/'");
			//rootFilePathName = "/Users/danielgoertz/Desktop/CRMEQ-ROOT";
		}else {
            File dummyFile = new File(args[0]);
            if (!dummyFile.exists() || !dummyFile.isDirectory()) {
                System.out.println("Path not exists or no directory, please try again.");
            }else{
                rootFilePathName = args[0];
            }
		}
		
		List<String> imageTypeWhiteList = Arrays.asList("jpg", "gif");
		List<DataModel> fileResultList = new ArrayList<>();

		FileManager fileManager = new FileManager(imageTypeWhiteList);

		try {
			fileResultList = fileManager.showContentOf(rootFilePathName);
		} catch (IOException e) {
			//e.printStackTrace();
            System.out.println(String.format("ERROR: Not able walk through the given path: %s ", rootFilePathName));
		}

		fileManager.updateResultList(fileResultList);
		for (DataModel element : fileResultList) {
			element.printDirResult();
		}
	}
}