/**
 * 
 */
package carmeqtest.controller;

import carmeqtest.model.DataModel;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Goertz
 *
 */
public class FileManager {

	private URLConnection urlConnection;
	private List<String> imageTypeWhiteList;
	private List<DataModel> fileResultList;
	private boolean entryPoint;

	public FileManager(List<String> imageTypeWhiteList) {
		this.imageTypeWhiteList = imageTypeWhiteList;
		this.fileResultList = new ArrayList<>();
		this.entryPoint = true;
	}

	/**
	 * @return the imageTypeWhiteList
	 */
	public List<String> getImageTypeWhiteList() {
		return imageTypeWhiteList;
	}

	/**
	 * @param imageTypeWhiteList the imageTypeWhiteList to set
	 */
	public void setImageTypeWhiteList(List<String> imageTypeWhiteList) {
		this.imageTypeWhiteList = imageTypeWhiteList;
	}

	/**
	 * @return the fileResultList
	 */
	public List<DataModel> getFileResultList() {
		return fileResultList;
	}

	/**
	 * @param fileResultList the fileResultList to set
	 */
	public void setFileResultList(List<DataModel> fileResultList) {
		this.fileResultList = fileResultList;
	}

	/**
	 * @return the entryPoint
	 */
	public boolean isEntryPoint() {
		return entryPoint;
	}

	/**
	 * @param entryPoint the entryPoint to set
	 */
	public void setEntryPoint(boolean entryPoint) {
		this.entryPoint = entryPoint;
	}

	/**
	 * @return the urlConnection
	 */
	public URLConnection getUrlConnection() {
		return urlConnection;
	}

	/**
	 * @param urlConnection the urlConnection to set
	 */
	public void setUrlConnection(URLConnection urlConnection) {
		this.urlConnection = urlConnection;
	}

	/**
	 * Simplest but not the efficient approach to detect the image resolution by using a BufferImage.
	 * Because the entire image will load into the memory.
	 * 
	 * @param file: File
	 * @return int[]: width and height
	 */
    public int[] getImageResolution(File file) {

		BufferedImage bimg = null;
		int width = 0;
		int height = 0;

		try {
			bimg = ImageIO.read(file);
		} catch (IOException e) {
			// e.printStackTrace();
			System.out.println(
					String.format("ERROR: Not able to read the given file: %s ", file.getAbsolutePath()));
		}

		try {
			width = bimg.getWidth();
			height = bimg.getHeight();
		} catch (NullPointerException e) {
			// e.printStackTrace();
			System.out.println(String.format("ERROR: Not able to detect image resolution of the given file: %s ",
					file.getAbsolutePath()));
		}
		return new int[] { width, height };
	}

	/**
	 * Calculate the compression ratio based on RGB image file of its
	 * file size and resolution. Honestly I don't know if that approach is correct
	 * without comparing the original (raw) file.
	 * 
	 * @param dataModel: DataModel
	 * @return int: Compression ratio
	 */
    public int getCompressionRatio(DataModel dataModel) {
		int numberOfColorChannels = 3;
		int compressionRatio = (int) ( (dataModel.getFileSize() / 1000 )* dataModel.getWidth()
				/ (dataModel.getWidth() * dataModel.getHeight() * numberOfColorChannels)) * 100;
		return compressionRatio;
	}

	/**
	 * Return the file extension of the given file path name.
	 * 
	 * @param filePathName: String to the absolute file path name.
	 * @return file extension
	 */
    public String getFileExtention(String filePathName) {
		String extension = "";

		int i = filePathName.lastIndexOf(".");
		if (i > 0) {
			extension = filePathName.substring(i + 1);
		}
		return extension.toLowerCase();
	}

	/**
	 * Update the entries in the list which represents as file type 'directory' to
	 * display the average of his content: file size, resolution and compression
	 * ratio.
	 * 
	 * @param fileResultList: List of DataModel objects
	 */
	public void updateResultList(List<DataModel> fileResultList) {
		int fileCounter = 0;
		int widthCounter = 0;
		int heightCounter = 0;
		int compressionCounter = 0;
		int fileSizeCounter = 0;

		for (DataModel outerElement : fileResultList) {
			if (outerElement.getDataType().equals("directory")) {
				for (DataModel innerElement : fileResultList) {
					if (innerElement.getParentDir().equals(outerElement.getFilePathName())
							&& !innerElement.getDataType().equals("directory")) {
						fileCounter += 1;
						fileSizeCounter += innerElement.getFileSize();
						compressionCounter += innerElement.getCompressionRatio();
						widthCounter += innerElement.getWidth();
						heightCounter += innerElement.getHeight();

						outerElement.setFileSize(fileSizeCounter / fileCounter);
						outerElement.setCompressionRatio(compressionCounter / fileCounter);
						outerElement.setWidth(widthCounter / fileCounter);
						outerElement.setHeight(heightCounter / fileCounter);
					}
				}
				fileCounter = 0;
				compressionCounter = 0;
				fileSizeCounter = 0;
				widthCounter = 0;
				heightCounter = 0;
			}
		}
	}

	/**
	 * Recursive function to walk through the given path to collect only
	 * directory and image files based on list imageTypeWhiteList. Each match is
	 * stored as a custom data object.
	 * 
	 * @param path: String from where to start walking through the directories
	 * @return list: List of DataModel objects
	 * @throws IOException Not able to open an URL/URI connection
	 */
	public List<DataModel> showContentOf(String path) throws IOException {

		File root = new File(path);
		File[] list = root.listFiles();

		if (list == null)
			return this.fileResultList;

		if (root.isDirectory() && this.entryPoint) {
			DataModel dataModel = new DataModel();
			dataModel.setFilePathName(root.getAbsolutePath());
			dataModel.setParentDir(root.getAbsolutePath());
			dataModel.setDataType("directory");
			this.fileResultList.add(dataModel);
			this.entryPoint = false;
		}

		for (File currentFile : list) {
			if (this.imageTypeWhiteList.contains(this.getFileExtention(currentFile.getAbsolutePath()))
					|| currentFile.isDirectory()) {

				this.urlConnection = currentFile.toURI().toURL().openConnection();
				String mimeType = this.urlConnection.getContentType();
				DataModel dataModel = new DataModel();
				dataModel.setFilePathName(currentFile.getAbsolutePath());

				if (currentFile.isDirectory()) {
					dataModel.setParentDir(currentFile.getAbsolutePath());
					dataModel.setDataType("directory");
					this.showContentOf(currentFile.getAbsolutePath());
				} else {
					int[] resolution = this.getImageResolution(currentFile);
					dataModel.setWidth(resolution[0]);
					dataModel.setHeight(resolution[1]);
					dataModel.setDataType(mimeType);
					dataModel.setFileSize(currentFile.length());
					dataModel.setCompressionRatio(this.getCompressionRatio(dataModel));
					dataModel.setParentDir(currentFile.getParent());
				}
				this.fileResultList.add(dataModel);
			}
		}
		return this.fileResultList;
	}
}
