/**
 * 
 */
package carmeqtest.model;


/**
 * @author Daniel Goertz
 * Object model to store the result and all.
 * necessary further information about the file.
 *
 */
public class DataModel {

	private String filePathName;
	private String dataType;
	private int compressionRatio;
	private int width;
	private int height;
	private long fileSize;
	private String parentDir;
	

	/**
	 * @return the parentDir
	 */
	public String getParentDir() {
		return parentDir;
	}

	/**
	 * @param parentDir the parentDir to set
	 */
	public void setParentDir(String parentDir) {
		this.parentDir = parentDir;
	}

	/**
	 * @return the fileSite
	 */
	public long getFileSize() {
		return fileSize;
	}

	/**
	 * @param fileSize the fileSite to set
	 */
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * @return the filePathName
	 */
	public String getFilePathName() {
		return filePathName;
	}

	/**
	 * @param filePathName the filePathName to set
	 */
	public void setFilePathName(String filePathName) {
		this.filePathName = filePathName;
	}

	/**
	 * @return the dataType
	 */
	public String getDataType() {
		return dataType;
	}

	/**
	 * @param dataType the dataType to set
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	/**
	 * @return the compressionRate
	 */
	public int getCompressionRatio() {
		return compressionRatio;
	}

	/**
	 * @param compressionRatio the compressionRate to set
	 */
	public void setCompressionRatio(int compressionRatio) {
		this.compressionRatio = compressionRatio;
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Method to print-out the result(s) in proper format.
	 */
	public void printDirResult() {

		System.out.println("-------------------------------");
		String nameFileTypeComment = String.format("name: %s, type: %s", this.filePathName, this.dataType);
		System.out.println(nameFileTypeComment);

		String fileSize = String.format("    file size:         %d Byte", this.fileSize);
		System.out.println(fileSize);

		String resolution = String.format("    resolution:        %d x %d Pixel", this.getWidth(), this.getHeight());
		System.out.println(resolution);

		String compressionRate = String.format("    compression ratio: %d%%", this.getCompressionRatio());
		System.out.println(compressionRate);
	}
}
